all: install start

start: run websocket_server

install: run
	docker-compose exec websocket-server bash -c "composer install"

run:
	docker-compose up -d

bash: run
	docker-compose exec websocket-server bash

websocket_server: run
	docker-compose exec -d websocket-server bash -c "bin/console gos:websocket:server"

websocket_server_logs: run
	docker-compose exec websocket-server bash -c "bin/console gos:websocket:server"

restart_websocket_server:
	docker-compose down
	docker-compose up -d
	docker-compose exec -d websocket-server bash -c "bin/console gos:websocket:server"

logs:
	docker-compose logs -ft
