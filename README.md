# Game backend

Allow multiplayer for "a" game.


## Install

``` bash
make install
make start
```


## License

This library is under [AGPL-v3 License](LICENSE).
